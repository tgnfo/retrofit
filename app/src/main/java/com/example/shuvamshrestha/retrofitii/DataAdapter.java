package com.example.shuvamshrestha.retrofitii;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder>{

    private ArrayList<AndroidVersion> android;

    public DataAdapter(ArrayList<AndroidVersion> android) {
        this.android = android;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_row,parent,false);
        return  new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.UserId.setText(Integer.toString(android.get(position).getUserId()));
        holder.Id.setText(Integer.toString(android.get(position).getId()));
        holder.Title.setText(android.get(position).getTitle());
        holder.Body.setText(android.get(position).getBody());
    }

    @Override
    public int getItemCount() {
        return android.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView UserId,Id,Title,Body;
        public ViewHolder(View itemView) {
            super(itemView);
            UserId = itemView.findViewById(R.id.userid);
            Id = itemView.findViewById(R.id.id);
            Title = itemView.findViewById(R.id.title);
            Body = itemView.findViewById(R.id.body);
        }
    }
}
