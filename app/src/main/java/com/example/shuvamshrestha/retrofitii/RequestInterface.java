package com.example.shuvamshrestha.retrofitii;

import retrofit2.Call;
import retrofit2.http.GET;


public interface RequestInterface {
    @GET("/posts/")
    Call<JsonResponse> getJSON();
}
